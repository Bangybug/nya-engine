//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#include "log.h"
#include "stdout_log.h"

namespace nya_log
{

namespace { log_base *current_log=new stdout_log(); }

log_base &no_log()
{
    static log_base *l=new log_base();
    return *l;
}

void set_log(log_base *l) { current_log=current_log?l:&no_log(); }
log_base &log() { return *current_log; }

}
