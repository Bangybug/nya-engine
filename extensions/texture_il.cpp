//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

//requires: libdevil http://openil.sourceforge.net/

#include "texture_il.h"
#include "scene/scene.h"

#include "IL/il.h"
#include "IL/ilu.h"

namespace nya_scene
{

bool load_texture_il(nya_scene::shared_texture &res,nya_scene::resource_data &data,const char* name)
{
    static bool once=true;
    if(once)
        ilInit(),once=false;

    const ILuint img=ilGenImage();
    ilBindImage(img);

    if(!ilLoadL(IL_TYPE_UNKNOWN,data.get_data(),data.get_size()))
    {
        ilDeleteImage(img);
        return false;
    }

    const ILint width=ilGetInteger(IL_IMAGE_WIDTH);
    const ILint height=ilGetInteger(IL_IMAGE_HEIGHT);
    const ILint bpp=ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
    const ILubyte *img_data=ilGetData();
    const ILint img_format=ilGetInteger(IL_IMAGE_FORMAT);

    ILinfo ImageInfo;
    iluGetImageInfo(&ImageInfo);
    if(ImageInfo.Origin==IL_ORIGIN_UPPER_LEFT)
        iluFlipImage();

    nya_render::texture::color_format format;
    switch(img_format)
    {
    case IL_LUMINANCE: format=nya_render::texture::greyscale; break;
    case IL_RGB: format=nya_render::texture::color_rgb; break;
    case IL_BGR: format=nya_render::texture::color_rgb; ilConvertImage(IL_RGB,IL_UNSIGNED_BYTE); break;
    case IL_RGBA: format=nya_render::texture::color_rgba; break;
    case IL_BGRA: format=nya_render::texture::color_bgra; break;

    default:
        nya_log::log()<<"load texture il error: incorrect format: "<<img_format<<" bpp "<<bpp<<"\n";
        ilDeleteImage(img);
        return false;
    }

    res.tex.build_texture(img_data,width,height,format);
    ilDeleteImage(img);
    texture::read_meta(res,data);
    return true;
}

}
