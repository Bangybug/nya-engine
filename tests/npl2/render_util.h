//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#pragma once

#include "render/platform_specific_gl.h"

namespace nya_render
{

inline void set_color(float r,float g,float b,float a)
{
 #ifndef DIRECTX11
  #ifdef ATTRIBUTES_INSTEAD_OF_CLIENTSTATES
   #ifndef NO_EXTENSIONS_INIT
    static PFNGLVERTEXATTRIB4FARBPROC glVertexAttrib4f=NULL;
    if(!glVertexAttrib4f)
        glVertexAttrib4f=(PFNGLVERTEXATTRIB4FARBPROC)get_extension("glVertexAttrib4fARB");
    #endif
    glVertexAttrib4f(color_attribute,r,g,b,a);
   #else
    glColor4f(r,g,b,a);
  #endif
 #endif
}

}
